///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage: Hello World
//
// @author Max Mochizuki <maxmm@hawaii.edu>
// @date   13 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>


int main(){

using namespace std;

cout << "Hello World!" << endl;

}
